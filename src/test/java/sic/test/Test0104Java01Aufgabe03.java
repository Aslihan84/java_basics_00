/**
 * Copyright (c) 2020-2024 Sven Gothel
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package sic.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * Siehe Java Grundelemente, JUC2 01.04 Java01.
 * <p>
 * Part 3: Basic algorithms with arrays and java.util.List
 * </p>
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class Test0104Java01Aufgabe03 {
    /**
     * Returns a new integer array of same length as the `input` array,
     * i.e. `input.length` while containing the same values but in reversed order.
     *
     * sizeof(int) == 4 (bytes)
     * int a[] = new int[4];
     * 0x0000: a[0]
     * 0x0004: a[1]
     * 0x0008: a[2]
     * 0x000C: a[3]
     *
     * @param input the given input array.
     * @return the newly created array containing input elements in reversed order
     */
    public static int[] reverse1(final int[] input) {
        // create a new array with dynamic size
        final int res[] = new int[input.length];

        /**
         *         index   0  1  2  3  4  5  6  7
         * int[] input = { 1, 2, 3, 4, 5, 6, 7, 8 }; // 8 elemente, index [0..7]         *
         * int[] res   = { 0, 0, 0, 0, 0, 0, 0, 0 }; // 8 elemente, index [0..7]         *
         *
         *     i  <-      j
         * res[0] = input[7]
         * res[1] = input[6]
         * res[2] = input[5]
         * res[3] = input[4]
         * res[4] = input[3]
         * res[5] = input[2]
         * res[6] = input[1]
         * res[7] = input[0]
         */
        for(int i=0; i<input.length; ++i) {
            res[i] = input[input.length-1-i];
        }
        return res; // FIXED: Return reversed input array
    }
    /** Same as reverse1() but returning a List<Integer> instead of int array */
    public static List<Integer> reverse2(final int[] input) {
        final List<Integer> list1 = new ArrayList<Integer>();
        for(int i=0; i<input.length; ++i) {
            list1.add( input[input.length-1-i] );
        }
        return list1; // FIXED: Return reversed input array
    }
    @Test
    void test01_reverse() {
        {
            final int[] src = { 1, 2, 3, 4, 5, 6, 7, 8 };
            final int[] exp = { 8, 7, 6, 5, 4, 3, 2, 1 };
            final int[] res = reverse1(src);
            Assertions.assertArrayEquals(exp, res);
        }
        {
            final int[] src = { 1, 2, 3, 4, 5, 6, 7, 8 };
            final List<Integer> exp = Arrays.asList(8, 7, 6, 5, 4, 3, 2, 1);
            final List<Integer> res = reverse2(src);
            Assertions.assertEquals(exp, res);
        }
    }

    //
    //
    //

    /**
     * Return `haystack` array index (starting from zero) of `value`
     * if found or `-1` (not found).
     *
     * The `haystack` is unsorted.
     *
     * Hint: Don't sort, i.e. use slow O(n) worst case complexity
     *
     * See test02_find_unsorted_b() and find_unsorted3() for usage.
     *
     * @param haystack the given unsorted haystack, i.e. array to search `value` in
     * @param value the needle to look out for in `haystack`
     * @return index if found, otherwise -1
     */
    public static int find_unsorted1(final char[] haystack, final char value) {
        /**
         *  index        0    1    2    3    4    5    6    7
         * char[] a = { 'w', 'r', 'z', 'a', 'p', 'b', 'u', 'v' }; // 8 elemente, index [0..7]         *
         * char value = 'b';
         *
         *   i
         * a[0]: w != b, nicht gefunden, weiter suchen
         * a[1]: r != b, nicht gefunden, weiter suchen
         * a[2]: z != b, nicht gefunden, weiter suchen
         * a[3]: a != b, nicht gefunden, weiter suchen
         * a[4]: p != b, nicht gefunden, weiter suchen
         * a[5]: b == b, !!! gefunden !!!, FERTIG (suche ist beendet) !!! return 5 (index)!
         */
        int i=0;
        while( i<haystack.length ) {
            if( value == haystack[i] ) {
                return i;
            }
            ++i;
        }
        return -1; // FIXED
    }
    /** Same as find_unsorted1() but using List<Integer> as the `haystack` instead of int array */
    public static int find_unsorted2(final List<Character> haystack, final char value) {
        int i=0;
        while( i<haystack.size()) {
            if( value == haystack.get(i) ) {
                return i;
            }
            ++i;
        }
        return -1; // FIXED
    }
    /** Same as find_unsorted2() but using existing List<Integer> operations */
    private static int find_unsorted3(final List<Character> haystack, final char value) {
        return haystack.indexOf(value); // implicity int -> Integer conversion
    }
    @Test
    void test02_find_unsorted() {
        final char[] haystack1 = { 'w', 'r', 'z', 'a', 'p', 'b', 'u', 'v' };
        final List<Character> haystack2 = Arrays.asList('w', 'r', 'z', 'a', 'p', 'b', 'u', 'v');
        Assertions.assertEquals(-1, find_unsorted1(haystack1, 'n'));
        Assertions.assertEquals(-1, find_unsorted2(haystack2, 'n'));

        Assertions.assertEquals(0, find_unsorted1(haystack1, 'w'));
        Assertions.assertEquals(0, find_unsorted2(haystack2, 'w'));

        Assertions.assertEquals(7, find_unsorted1(haystack1, 'v'));
        Assertions.assertEquals(7, find_unsorted2(haystack2, 'v'));

        Assertions.assertEquals(4, find_unsorted1(haystack1, 'p'));
        Assertions.assertEquals(4, find_unsorted2(haystack2, 'p'));
    }
    @Test
    void test02_find_unsorted_b() {
        final List<Character> haystack2 = Arrays.asList('w', 'r', 'z', 'a', 'p', 'b', 'u', 'v');
        Assertions.assertEquals(-1, find_unsorted3(haystack2, 'n'));

        Assertions.assertEquals(0, find_unsorted3(haystack2, 'w'));

        Assertions.assertEquals(7, find_unsorted3(haystack2, 'v'));

        Assertions.assertEquals(4, find_unsorted3(haystack2, 'p'));
    }

    //
    //
    //

    /**
     * Zahlenraten [1..10]:
     * A: 7 (geheim) -> [1..10]
     * 
     * B: 5?
     * A: Groesser! -> [6..10]
     * 
     * B: 8?
     * A: Kleiner! -> [6..7]
     * ..
     * ..
     */
    
    /**
     * Return `haystack` array index (starting from zero) of `value`
     * if found or `-1` (not found).
     *
     * The `haystack` is sorted.
     *
     * Hint: Use binary search for O(log n) complexity (performance)
     *
     * See test03_find_sorted_b() and find_sorted3() for usage.
     *
     * @param haystack the given sorted haystack, i.e. array to search `value` in
     * @param value the needle to look out for in `haystack`
     * @return index if found, otherwise -1
     */
    public static int find_sorted1(final int[] haystack, final int value) {
        int l = 0;
        int h = haystack.length-1;
        // Suchbereich [l..h] -> inclusive
        if ( haystack[l] == value ) {
            return l;
        } else if ( haystack[h] == value ) {
            return h;
        }
        // Suchbereich ]l..h[ (l..h) -> exclusive
        while( h - l >= 2 ) {
            // int i = ( l + h ) / 2; // l+h too big?
            final int i = l + ( h - l ) / 2; // better, also solved with std::midpoint(l, h)
            if ( haystack[i] < value ) {
                l = i;
            } else if ( haystack[i] > value ) {
                h = i;
            } else {
                return i;
            }
        }
        return -1; // FIXED
    }
    /** Same as find_sorted1() but using List<Integer> as the `haystack` instead of int array */
    public static int find_sorted2(final List<Integer> haystack, final int value) {
        int l = 0;
        int h = haystack.size()-1;
        if ( haystack.get(l) == value ) {
            return l;
        } else if ( haystack.get(h) == value ) {
            return h;
        }
        while( h - l >= 2 ) {
            // int i = ( l + h ) / 2; // l+h too big?
            final int i = l + ( h - l ) / 2; // better, also solved with std::midpoint(l, h)
            if ( haystack.get(i) < value ) {
                l = i;
            } else if ( haystack.get(i) > value ) {
                h = i;
            } else {
                return i;
            }
        }
        return -1; // FIXED
    }
    /** Same as find_sorted1() but using existing Arrays's binarySearch operations */
    private static int find_sorted3(final int[] haystack, final int value) {
        final int idx = Arrays.binarySearch(haystack, value);
        return 0 <= idx ? idx : -1;
    }
    @Test
    void test03_find_sorted() {
        final int[] haystack1 = { 1, 2, 3, 4, 5, 6, 7, 8 };
        final List<Integer> haystack2 = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8);
        Assertions.assertEquals(-1, find_sorted1(haystack1, 9));
        Assertions.assertEquals(-1, find_sorted2(haystack2, 9));

        Assertions.assertEquals(0, find_sorted1(haystack1, 1));
        Assertions.assertEquals(0, find_sorted2(haystack2, 1));

        Assertions.assertEquals(7, find_sorted1(haystack1, 8));
        Assertions.assertEquals(7, find_sorted2(haystack2, 8));

        Assertions.assertEquals(4, find_sorted1(haystack1, 5));
        Assertions.assertEquals(4, find_sorted2(haystack2, 5));
    }

    @Test
    void test03_find_sorted_b() {
        final int[] haystack1 = { 1, 2, 3, 4, 5, 6, 7, 8 };
        Assertions.assertEquals(-1, find_sorted3(haystack1, 9));

        Assertions.assertEquals(0, find_sorted3(haystack1, 1));

        Assertions.assertEquals(7, find_sorted3(haystack1, 8));

        Assertions.assertEquals(4, find_sorted3(haystack1, 5));
    }

    //
    //
    //

    /**
     * Remove `value` from `haystack` and return new reduced haystack if value found and removed,
     * otherwise null.
     *
     * The `haystack` is sorted.
     *
     * See test04_remove_sorted_b() and remove_sorted3() for usage.
     *
     * @param haystack the given sorted haystack, i.e. array from which `value` shall be removed
     * @param value the element which shall be removed from `haystack`
     * @return new reduced haystack if value found and removed, otherwise null
     */
    public static int[] remove_sorted1(final int[] haystack, final int value) {
        final int idx = find_sorted1(haystack, value);
        if( 0 > idx ) {
            return null;
        }
        final int[] res = new int[haystack.length-1];
        int j=0;
        for(int i=0; i<idx; ++i) {
            res[j++] = haystack[i];
        }
        // Skip haystack[idx] !
        for(int i=idx+1; i<haystack.length; ++i) {
            res[j++] = haystack[i];
        }
        return res; // FIXED
    }
    /** 
     * Similar to remove_sorted1() but using List<Integer> as the `haystack` instead of int array
     * and modifying the `haystack` directly.
     */
    public static boolean remove_sorted2(final List<Integer> haystack, final int value) {
        final int idx = find_sorted2(haystack, value);
        if( 0 > idx ) {
            return false;
        }
        // shift right-elements one to the left, erasing element at idx
        for(int i=idx+1; i<haystack.size(); ++i) {
            haystack.set(i-1, haystack.get(i));
        }
        haystack.remove(haystack.size()-1); // remove last
        return true; // FIXED
    }
    /** Same as remove_sorted2() but using existing List<Integer> operations */
    private static boolean remove_sorted3(final List<Integer> haystack, final int value) {
        final int index = haystack.indexOf(value);
        if( 0 > index ) {
            return false;
        }
        final Integer removed = haystack.remove(index);
        Assertions.assertNotNull(removed);
        Assertions.assertEquals(removed.intValue(), value);
        return true;
    }
    @Test
    void test04_remove_sorted() {
        int[] haystack1 = { 1, 2, 3, 4, 5, 6, 7, 8 };
        // Note: List<Integer>'s remove operation is not supported on the
        //       return array backed List<Integer> implementation of Arrays.asList(..).
        //       Hence we have to use ArrayList<Integer> and add all elements.
        final List<Integer> haystack2 = new ArrayList<Integer>();
        haystack2.addAll( Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8) );
        {
            Assertions.assertNull(remove_sorted1(haystack1, 9));
            Assertions.assertEquals(false, remove_sorted2(haystack2, 9));
        }
        {
            final int[] exp1 = { 2, 3, 4, 5, 6, 7, 8 };
            final List<Integer> exp2 = Arrays.asList(2, 3, 4, 5, 6, 7, 8);

            haystack1 = remove_sorted1(haystack1, 1);
            Assertions.assertArrayEquals(exp1, haystack1);

            Assertions.assertEquals(true, remove_sorted2(haystack2, 1));
            Assertions.assertEquals(exp2, haystack2);
        }
        {
            final int[] exp1 = { 2, 3, 4, 5, 6, 7 };
            final List<Integer> exp2 = Arrays.asList(2, 3, 4, 5, 6, 7);

            haystack1 = remove_sorted1(haystack1, 8);
            Assertions.assertArrayEquals(exp1, haystack1);

            Assertions.assertEquals(true, remove_sorted2(haystack2, 8));
            Assertions.assertEquals(exp2, haystack2);
        }
    }
    @Test
    void test04_remove_sorted_b() {
        // Note: List<Integer>'s remove operation is not supported on the
        //       return array backed List<Integer> implementation of Arrays.asList(..).
        //       Hence we have to use ArrayList<Integer> and add all elements.
        final List<Integer> haystack2 = new ArrayList<Integer>();
        haystack2.addAll( Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8) );
        {
            Assertions.assertEquals(false, remove_sorted3(haystack2, 9));
        }
        {
            final List<Integer> exp2 = Arrays.asList(2, 3, 4, 5, 6, 7, 8);

            Assertions.assertEquals(true, remove_sorted3(haystack2, 1));
            Assertions.assertEquals(exp2, haystack2);
        }
        {
            final List<Integer> exp2 = Arrays.asList(2, 3, 4, 5, 6, 7);

            Assertions.assertEquals(true, remove_sorted3(haystack2, 8));
            Assertions.assertEquals(exp2, haystack2);
        }
    }

}
